"""This module is the main transform function for Twitter's API """

def process(data, config):
  """
  This method do the transformation process for the Twitter API response
  """

  print('Init twitter transform function')
  print(config)
  import json

  json_data = json.loads(data)
  return json_data
