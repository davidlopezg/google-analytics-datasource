export enum methodType {
    Get = "GET",
    Post = "POST"
}

export interface GoogleAnalyticsParams {
    start_date? : string,
    end_date? : string,
    credentials : {
        client_secret: string,
        client_id: string,
        redirect_uris: any,
    },
    metrics: any,
    dimensions: any,
    channells: any,
    method : methodType,
    path? : string,
    ds_name? : string,
    config? : any,
    run_id? : string,
    params? : any
}