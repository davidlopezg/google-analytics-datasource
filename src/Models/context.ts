/**
 * Interface for context model
 */
export interface Context {
    done(data ? : any) : void;
    error(error ? : any) : void;
};