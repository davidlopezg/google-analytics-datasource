import * as fs from 'fs';
import * as readline from 'readline';
import {google} from 'googleapis';
import {GoogleAnalyticsParams} from './Models/params';

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/google-analytics-nodejs-quickstart.json
const OAuth2 = google.auth.OAuth2;
const SCOPES = ['https://www.googleapis.com/auth/analytics.readonly'];
const TOKEN_DIR = './assets/';
const TOKEN_PATH = TOKEN_DIR + 'google-analytics-nodejs-quickstart.json';

export class GoogleAnalyticsHelper {
  /**
   * Constructor
   */
  constructor() { }

  /**
   * Method used for call the Twitter API
   * @param  {GoogleAnalyticsParams} event Required parameters to process the event
   * @return {any} Promise
   */
  processEvent(event: GoogleAnalyticsParams): any {
    const self: GoogleAnalyticsHelper = this;

    const credentials = event.credentials;
    const clientSecret = credentials.client_secret;
    const clientId = credentials.client_id;
    const redirectUrl = credentials.redirect_uris[0];
    const oauth2Client = new OAuth2(clientId, clientSecret, redirectUrl);
    const parameters = event.params || {};

    return new Promise((resolve, reject) => {
      // Check if we have previously stored a token.
      fs.readFile(TOKEN_PATH, (err: any, token: any) => {
        if (err) {
          self.getNewToken(oauth2Client, (token: any) => {
            self.getReport(token, parameters)
              .then((result: any)=> resolve(result))
              .catch((error: any) => reject(error));
          });
        } else {
          oauth2Client.credentials = JSON.parse(token);
          self.getReport(oauth2Client, parameters)
            .then((result: any)=> resolve(result))
            .catch((error: any) => reject(error));
        }
      });
    });
  };

  /**
   * Get and store new token after prompting for user authorization, and then
   * execute the given callback with the authorized OAuth2 client.
   *
   * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
   * @param {getEventsCallback} callback The callback to call with the authorized
   *     client.
   */
  getNewToken(oauth2Client: any, callback: any) {
    const self: GoogleAnalyticsHelper = this;
    const authUrl = oauth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url: ', authUrl);
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', function (code: string) {
      rl.close();
      oauth2Client.getToken(code, function (err: any, token: any) {
        if (err) {
          console.log('Error while trying to retrieve access token', err);
          return;
        }
        oauth2Client.credentials = token;
        self.storeToken(token);
        callback(oauth2Client);
      });
    });
  };

  /**
   * Store token to disk be used in later program executions.
   *
   * @param {Object} token The token to store to disk.
   */
  storeToken(token: string) {
    // try {
    //   fs.mkdirSync(TOKEN_DIR);
    // } catch (err) {
    //   if (err.code != 'EXIST')
    //     throw err;
    // }
    fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err: any) => {
      if (err) throw err;
      console.log('Token stored to ' + TOKEN_PATH);
    });
    console.log('Token stored to ' + TOKEN_PATH);
  };

  /**
   * Lists the names and IDs.
   *
   * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
   * @param {any} params Event configuration
   * @return {Promise} Return a promise with the report data
   */
  getReport(auth: any, params: any) : any {
    return new Promise((resolve, reject) => {
      const googleAnalyticsReporting = google.analyticsreporting({version: 'v4', auth: auth});

      googleAnalyticsReporting.reports.batchGet({
        requestBody: params})
        .then((result) =>
          resolve(result))
        .catch((error) =>
          reject(error));
    });
  };
}
