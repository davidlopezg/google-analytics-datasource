import {GoogleAnalyticsHelper} from './google_analytics_helper';
import {GoogleAnalyticsParams} from './Models/params';
import * as payloadHelper from './payload_helper';

/**
* Main function for lambda instace
* @param {GoogleAnalyticsParams} event Evento
* @return {Promise} Retorna una promesa
*/
export function handler(event: GoogleAnalyticsParams) {
  console.log('Init Lambda execution!!!');
  const googleAnalyticsHelper: GoogleAnalyticsHelper = new GoogleAnalyticsHelper();
  const config = JSON.parse(event.config);

  const eventConf : any = {
    ds_name: event.ds_name,
    current_step: 'init',
    config: config,
    run_id: event.run_id,
    data: '',
  };

  return new Promise((resolve, reject) => {
    googleAnalyticsHelper.processEvent(event)
      .then((result : any) => {
        console.log('processEvent OK : ', JSON.stringify(result));

        eventConf.data = JSON.stringify([{report: result}]);
        payloadHelper.sendPayload(eventConf, function(error) {
          if (error) reject(error);
          else resolve(result);
        });
      })
      .catch((error : any) => {
        console.log('processEvent ERROR : ', JSON.stringify(error));
        reject(error);
      });
  });
};
