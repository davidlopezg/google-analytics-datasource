import {Lambda, S3, DynamoDB} from 'aws-sdk';
const jsonSize = require('json-size');
import * as async from 'async';

const lambda: Lambda = new Lambda({region: 'us-east-1'});
const s3: S3 = new S3({region: 'us-east-1'});
const dynamodb: DynamoDB = new DynamoDB({region: 'us-east-1'});

const OBJECT_LIMIT : number = 5000000;
const PROCESS_LAMBDA : any = {python: 'python-preprocess-lambda', nodejs: 'python-preprocess-lambda'};
const BUCKET_NAME : string = 'winsights-data';

/**
 * Payload helper main method
 *
 * @param {any} payload
 * @param {Function} callback
 */
export function sendPayload(
  payload: any,
  callback: (error?: any, result?: any) => void
): void {
  try {
    let configResult : any = null;
    const configError : any = null;

    async.waterfall(
      [
        (cb: any) => {
          getConfigData(payload.ds_name, cb);
        },
        (data: any, cb: any) => {
          configResult = data;
          getFunctionDetail(configResult.Item, cb);
        },
        (runtimeEnv: any, cb: any) => {
          processData(payload, configError, configResult, runtimeEnv, cb);
        },
      ],
      callback
    );
  } catch (error) {
    console.log('sendPayload Exception:', error);
    throw (error);
  }
}

/**
 * Method used for process all the data after pre process
 * @param {any} payload
 * @param {any} error
 * @param {any} data
 * @param {string} runtimeEnv
 * @param {Function} callback
 */
function processData(
  payload: any,
  error: any,
  data: any,
  runtimeEnv: string,
  callback: (error?: any, result?: any) => void
) : void {
  try {
    const objectSize = jsonSize(payload);
    const runtime = runtimeEnv || 'python';
    if (objectSize > OBJECT_LIMIT) {
      if (error) {
        console.log(error);
        callback('Error while getting config data of ' + payload.ds_name, null);
      } else {
        // Send it through S3 Bucket
        const key = 'datasource_inputs/' + data.Item.wclient_id.S + '/' + data.Item.name.S + '/input_' + (+ new Date()) + '.json';
        const params = {
          Body: JSON.stringify(payload),
          Bucket: BUCKET_NAME,
          Key: key,
        };
        s3.putObject(params, function (err, data) {
          if (err) {
            console.log(err, err.stack); // an error occurred
            callback(err);
          } else {
            payload.data = '[]';
            payload.type = 's3';
            payload.ds_input = key;
            // Send it through lambda
            const params = {
              FunctionName: PROCESS_LAMBDA[runtime],
              InvocationType: 'RequestResponse',
              Payload: JSON.stringify(payload),
            };
            lambda.invoke(params, function (err, data) {
              if (err) {
                console.log(err, err.stack); // an error occurred
                callback(err);
              } else {
                callback(null, 'success');
              }
            });
          }
        });
      }
    } else {
      // Send it through lambda
      console.log(typeof payload, payload);
      payload.type = 'event';
      const params = {
        FunctionName: PROCESS_LAMBDA[runtime],
        InvocationType: 'RequestResponse',
        Payload: JSON.stringify(payload),
      };
      lambda.invoke(params, function (err, data) {
        if (err) {
          console.log(err, err.stack); // an error occurred
          callback(err);
        } else {
          callback(null, 'success');
        }
      });
    }
  } catch (error) {
    console.log('processData Exception:', error);
    throw (error);
  }
}

/** *************************************************************** */
// Get Config Data
/** *************************************************************** */

/**
 * Get config by datasource name
 * @param {string} datasource datasource name
 * @param {Function} callback result
 */
function getConfigData(
  datasource: string,
  callback: (error?: any, result?: any) => void
): void {
  try {
    const queryparams = {
      Key: {
        name: {
          S: datasource,
        },
      },
      TableName: 'winsights-client-datasources',
    };

    dynamodb.getItem(queryparams, function (err, data : DynamoDB.Types.GetItemOutput) {
      if (err) {
        callback(err, null);
      } else {
        data.Item ?
          callback(null, data) :
          callback(`Cannot find datatsource with name : ${datasource}`);
      }
    });
  } catch (error) {
    console.log('getConfigData Exception:', error);
    throw (error);
  }
}

/** *************************************************************** */
// Get Function Detail
/** *************************************************************** */

/**
 * Get first preconfig function details to select next runtime
 * @param {any} itemResponse
 * @param {Function} callback
 */
function getFunctionDetail(
  itemResponse: any,
  callback: (error?: any, result?: any) => void
): void {
  try {
    if (itemResponse.pre_config.L.length == 0) {
      callback(null);
    } else {
      const functionDetails = itemResponse.pre_config.L.filter((step: any) => step.M.order.S == '1');
      if (functionDetails === undefined || functionDetails.length == 0)
        callback(null);
      else
        callback(null, functionDetails[0].M.runtime.S);
    }
  } catch (error) {
    console.log('getFunctionDetail Exception:', error);
    throw (error);
  }
}


/** *************************************************************** */
// Update Datasource Path
/** *************************************************************** */

/**
 * Update S3 path in datasource config
 * @param {string} runId
 * @param {any} type
 * @param {string} path
 * @param {Function} callback
 */
// function updateDatasourcePath(
//   runId: string,
//   type: any,
//   path: string,
//   callback: (error?: any, result?: any) => void
// ): void {
//   try {
//     const params = {
//       ExpressionAttributeNames: {
//         '#DS_INPUT': 'ds_input',
//         '#TYPE': 'type',
//       },
//       ExpressionAttributeValues: {
//         ':dsi': {
//           S: path,
//         },
//         ':t': {
//           S: type,
//         },
//       },
//       Key: {
//         run_id: {
//           S: runId,
//         },
//       },
//       ReturnValues: 'ALL_NEW',
//       TableName: 'datasource-run-history',
//       UpdateExpression: 'SET #DS_INPUT = :dsi, #TYPE = :t',
//     };
//     dynamodb.updateItem(params, function (err, data) {
//       if (err) callback(err); // an error occurred
//       else callback(null, data); // successful response
//     });
//   } catch (error) {
//     console.log('updateDatasourcePath Exception:', error);
//     throw (error);
//   }
// }